* https://www.manning.com/books/functional-programming-in-scala
* https://effectivesoftwaredesign.com/2012/11/25/on-information-hiding-and-encapsulation/
* https://en.wikipedia.org/wiki/Information_hiding
* https://en.wikipedia.org/wiki/Encapsulation_(computer_programming)
* http://www.cs.umd.edu/class/spring2003/cmsc838p/Design/criteria.pdf
* http://stefanoricciardi.com/2009/12/06/encapsulation-and-information-hiding/
* http://www.c2.com/cgi/wiki?EncapsulationIsNotInformationHiding
* http://stackoverflow.com/questions/24626/abstraction-vs-information-hiding-vs-encapsulation
* [Paul's Sidebar](https://gist.github.com/pchiusano/48a4fb6bc301e6d9c25c)
* [he likes it](https://twitter.com/pchiusano/status/490238198568599552)
* [Runar's Paper](http://days2012.scala-lang.org/sites/days2012/files/bjarnason_trampolines.pdf)
* https://devchat.tv/ruby-rogues/065-rr-functional-vs-object-oriented-programming-with-michael-feathers
* http://mathoverflow.net/questions/16180/formalizing-no-junk-no-confusion

## Style

* https://gitlab.innoq.com/daniel/innoq-slides-go-present-template
* https://github.com/innoq/innoq-styles
* https://internal.innoq.com/blogging/blogs/37/entries/1005335
* https://gitlab.innoq.com/innoq/web-arch-workshop
* https://github.com/innoq/innoq-slide-master
* https://gitlab.innoq.com/innoq/folien-master