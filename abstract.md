# The Hidden Nature of Data

Encapsulation is a pillar of object-oriented programming, but how important is it in the context of functional programming? Is it worth clinging to, or should we stay away? This talk will examine the tradeoffs of encapsulating internal representations versus exposing the "nature" of our data.

Programming with immutable values and algebraic data types usually makes hiding fields behind accessors unnecessary. On the other hand, hiding functions and types is a crucial technique control our API, encouraging correct usage and affording the flexibility to change it.

In this session we will explore constraints that encourage encapsulation, discuss how it affects our types, their algebras, and their algorithmic properties, and learn about alternatives and their benefits and drawbacks.

## Takeaway

* What are benefits and drawbacks of encapsulating data?
* How does hidden representation affect my algebra and my code?
* Should I prefer pattern matching or folding?
