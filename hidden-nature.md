<!-- .slide: class="presentation-title" -->
# The Hidden Nature <br> of Data
## BOBKonf 2017
## Martin Kühl, [@mkhl](https://twitter.com/mkhl)

<img src="node_modules/innoq-styles/images/logo_172@2x.png" width="172px">

<!-- .element: class="logo" -->

---

<!-- .slide: class="title-only" -->
<img src="images/Zora.jpg" width="900px">

---

<!-- .slide: class="title-only" -->
## Motivation

^ Wie bin ich darauf gekommen, diesen Vortrag zu halten?

---

<img src="images/fpinscala.jpg" height="600px">
<br>
Paul Chiusano and Rúnar Bjarnason

^ Es gibt da ein sehr gutes Buch über funktionale Programmierung, von Paul und Rúnar…

---

> **One might object that algebraic data types violate encapsulation** by making public the internal representation of a type. […] **Exposing the data constructors of a type is often fine**, and the decision to do so is approached much like any other decision about what the public API of a data type should be.

[pchiusano-sidebar]: https://gist.github.com/pchiusano/48a4fb6bc301e6d9c25c

^ …das enthält diese Randnotiz

---

<img src="images/pchiusano.png" width="1000px">

[pchiusano-like]: https://twitter.com/pchiusano/status/490238198568599552

^ und Paul findet sie immer noch gut

---

<!-- .slide: class="title-only" -->
## But

---

## Stackless Scala With Free Monads, Rúnar Bjarnason

[stackless-scala]: http://days2012.scala-lang.org/sites/days2012/files/bjarnason_trampolines.pdf

~~~
sealed trait Free[S[+_],+A] {
  private case class FlatMap[S[+_],A,+B](
    a: Free[S,A],
    f: A => Free[S,B]) extends Free[S,B]
}
~~~

^ aber Rúnar hat (schon früher) dieses Paper geschrieben…

…das enthält diese Instanz eines absichtlich versteckten Datenkonstruktors

Der Code deklariert einen internen Konstruktor `FlatMap` mit derselben Signatur wie `Free#flatMap`.

Im Paper wird der Grund hergeleitet: `flatMap` lässt sich nicht endrekursiv implementieren und ist deshalb nicht “stack safe”, muss deshalb als Datenkonstruktor abgebildet werden.

Konflikt zwischen mathematischen Strukturen und dem Alltag mit der Maschine, bedroht hier die Korrektheit der Implementierung

---

<!-- .slide: class="title-only" -->
## So when is exposing our data constructors **not** fine? <br> And what does that mean for when we should use pattern matching?

^ Eine Seite: ADTs mit öffentlichen Datenkonstruktoren, Verarbeitung mit Pattern Matching, Zugriff auf *Struktur*

Andere Seite: opake Typen mit privaten Datenkonstruktoren, Verarbeitung mit Fold (z.B.), Zugriff auf *Interface*

---

<!-- .slide: class="title-only" -->
## Example

---

## Example: List

~~~scala
case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]

sealed trait List[+A] {
  def fold[B](z: B)(f: (A, B) => B): B = this match {
    case Nil        => z
    case Cons(h, t) => f(h, t.fold(z)(f))
  }

  def append[B >: A](that: List[B]): List[B] =
  	this.fold(that)(Cons(_, _))
~~~

^ Stell Dir vor, Du hättest eine Anwendung geschrieben, die verwendet diesen grossartigen neuen Typen `List`, und zwar **überall**.

Dir ist aufgefallen, dass `List` ein Monoid ist, dass die Operation (`append`) aber lineare Zeit braucht.

Ebenfalls Konflikt zwischen mathematischer Struktur und Realität der Maschine, hier bereitet uns mangelnde Effizienz Sorgen

Da kommst Du auf die Idee, `append` hochzuheben und einen Datenkonstruktor daraus zu machen:

---

## Example: List + Append

~~~
case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]
private case class Append[+A](left: List[A], right: List[A]) extends List[A]

sealed trait List[+A] {
  def fold[B](z: B)(f: (A, B) => B): B = this match {
    case Nil          => z
    case Cons(h, t)   => f(h, t.fold(z)(f))
    case Append(l, r) => l.fold(r.fold(z)(f))(f)
  }

  def append[B >: A](that: List[B]): List[B] = (this, that) match {
    case (_, Nil) => this
    case (Nil, _) => that
    case _        => Append(this, that)
  }
~~~

^ Das scheint auch zu klappen, und Du brauchst nicht mal das Interface von `fold` zu ändern! Der zusätzliche Datenkonstruktor ist versteckt.

`fold` spielt hier ähnliche Rolle wie Getter: bietet Punkt, an dem der Typ sein Verhalten bestimmen kann, ohne seine Repräsentation anderem Code zu überlassen

---

<!-- .slide: class="title-only" -->
## Problems

---

## It breaks pattern matching

~~~
[warn] /path/to/Service.scala:28: match may not be exhaustive.
[warn] It would fail on the following input: Append(_, _)
[warn]     def go(as: List[A], count: Int): Int = as match {
[warn]                                             ^
~~~

^ Ohne zusätzliche Compiler-Flags sogar erst zur *Laufzeit*!

…aber damit mussten wir natürlich rechnen

---

<!-- .slide: class="title-only" -->
## It violates “No Confusion”

^ Was sind Annahmen, die wir über unsere Datentypen treffen wollen?

---

> No Junk, No Confusion

> Joseph A. Goguen
<!-- .element: class="quote-source" -->

<div style="text-align: center;">
	<img src="images/no-junk.svg" width="26%">
	<img src="images/no-confusion.svg" width="26%">
</div>

^ Datentypen beschreiben Kodierung von mathematischen Strukturen für Maschinen

No Junk: Jede Kombination von Konstruktoren beschreibt eine *korrekte* Instanz, ansonsten lassen wir unser Typsystem uns belügen

No Confusion: Jede Instanz wird durch *genau eine* Kombination von Konstruktoren beschrieben, ansonsten machen wir aus strukturellen Problemen semantische Probleme (Gleichheit) und verhindern effiziente Implementierung (Gleichheitsprüfung *ohne* Allokation)

---

<!-- .slide: class="title-only" -->
## It changes our algebra

^ Wir arbeiten jetzt auf einem Typen mit einer anderen Struktur, einer anderen *Natur*, müssen uns an diese anpassen.

Deshalb geht Pattern Matching kaputt, ändert sich Laufzeitverhalten.

Stabil ist nur `fold` geblieben, wir können also jenseits von `fold` keine Annahmen treffen.

---

<!-- .slide: class="title-only" -->
## Advice

---

> Be conservative in what you do, be liberal in what you accept from others.

> Jon Postel
<!-- .element: class="quote-source" -->

^ Accept generic types as parameters, return specific types as results

---

<!-- .slide: class="title-only" -->
## Is your library providing a language or interpreting one?

^ Language as in the interpreter pattern: a structure of symbols to be interpreted

A library’s primary purpose is usually
* either to provide a structure
* or to *do stuff*, possibly with a custom structure

---

<!-- .slide: class="title-only" -->
## If you are providing a language, try to expose its structure

^ The language *is* a structure of symbols, exposing its structure affords the interpreter more flexibility

---

<!-- .slide: class="title-only" -->
## If you are interpreting a language, try consuming interfaces

^ If you are providing the interfaces, you retain flexibility to adapt internal structures.

If you aren’t, you afford the provider the same flexibility

---

<!-- .slide: class="title-only" -->
## If you are introducing interfaces, don’t replace existing structures

<!-- ## Extract the interfaces, adapt the structures with typeclasses -->

^ Wenn wir vorher offene Datentypen durch Interfaces *ersetzen*, gibt es Probleme

Beispiel: (List ist ein Typ für verkettete Listen)
* nö: List => List, ArrayList, LinkedList
* ja: List => IndexedSequence, Array, List

---

<!-- .slide: class="title-only" -->
## Changes to existing structures **should** affect downstream code

^ Wenn es enge fachliche Zusammenhänge gibt, sollten diese entweder explizit geteilt oder explizit abgeschirmt sein. s.a. Domain Driven Design: Context Map Patterns (z.B. Anticorruption Layer)

---

<!-- .slide: class="title-only" -->
## Pattern matching can be **nested**

---

## Pattern matching can be **nested**

~~~
case (Some(Foo(x)), Right(Bar(y))) => …
~~~

^ Verschachteltes Pattern Matching spart erstaunlich viel Gymnastik

---

## More flexibility in defining types

~~~
case class Prog[A](get: Free[Lang, A])
~~~

vs.

~~~
type Prog[A] = Free[Lang, A]
~~~

^ Für den zweiten Typ können wir `fold` nicht definieren, weil es die Methode schon gibt, müssen also wrappen (ersten Typ nehmen)

---

<!-- .slide: class="title-only" -->
## Pattern matching <br> Try to use and support it

---

<!-- .element: class="bigger" -->

<p style="float: right; line-height: initial; width: 35%;">
[@mkhl](https://twitter.com/mkhl)<br>
<span class="smaller">Martin Kühl</span><br>
<span class="smaller">[martin.kuehl@innoq.com](mailto:martin.kuehl@innoq.com)</span>
</p>

Thank you!

Questions?

Comments?

<img src="node_modules/innoq-styles/images/logo_172@2x.png" width="172px">

<!-- .element: class="logo" -->
