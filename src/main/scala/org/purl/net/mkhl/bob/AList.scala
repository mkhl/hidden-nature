/*
 * Copyright (c) 2017 Martin Kühl
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.purl.net.mkhl.bob

sealed trait AList[+A] {
  import AList._

  def isEmpty: Boolean = this match {
    case ANil         => true
    case ACons(_, _)  => false
    case Append(l, r) => l.isEmpty && r.isEmpty
  }

  def fold[B](z: B)(f: (A, B) => B): B = this match {
    case ANil         => z
    case ACons(h, t)  => f(h, t.fold(z)(f))
    case Append(l, r) => l.fold(r.fold(z)(f))(f)
  }

  def map[B](f: A => B): AList[B] = this.fold(nil[B])((h, t) => ACons(f(h), t))

  def append[B >: A](that: AList[B]): AList[B] = (this, that) match {
    case (_, ANil) => this
    case (ANil, _) => that
    case _         => Append(this, that)
  }
}

object AList {
  def nil[A]: AList[A] = ANil
}

case object ANil                              extends AList[Nothing]
case class ACons[+A](head: A, tail: AList[A]) extends AList[A]
private[this] case class Append[+A](left: AList[A], right: AList[A])
    extends AList[A]
