/*
 * Copyright (c) 2017 Martin Kühl
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.purl.net.mkhl.bob

sealed trait List[+A] {
  import List._

  def isEmpty: Boolean = this match {
    case Nil        => true
    case Cons(_, _) => false
  }

  def fold[B](z: B)(f: (A, B) => B): B = this match {
    case Nil        => z
    case Cons(h, t) => f(h, t.fold(z)(f))
  }

  def map[B](f: A => B): List[B] = this.fold(nil[B])((h, t) => Cons(f(h), t))

  def append[B >: A](that: List[B]): List[B] = this.fold(that)(Cons(_, _))
}

object List {
  def nil[A]: List[A] = Nil
}

case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]
