DOT := dot 

DOTS := $(wildcard images/*.dot)
PNGS := $(patsubst %.dot,%.png,$(DOTS))
PDFS := $(patsubst %.dot,%.pdf,$(DOTS))
SVGS := $(patsubst %.dot,%.svg,$(DOTS))

all: $(SVGS)

%.png: %.dot
	$(DOT) -Tpng -o $@ $<

%.pdf: %.dot
	$(DOT) -Tpdf -o $@ $<

%.svg: %.dot
	$(DOT) -Tsvg -o $@ $<
